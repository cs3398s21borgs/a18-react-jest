import React from 'react';
import sum from './sum.js'

test('adds 1 + 2 to equal 3', () => {
  expect(sum(1, 2)).toBe(3);
});

// Logan Hodgins A18 test
test('adds 15 + 33 to equal 48', () => {
  expect(sum(15, 33)).toBe(48);
});

// Logan Hodgins A18 different test
test('adds 15 + 33 to not equal 148', () => {
  expect(sum(15, 33)).not.toBe(148);
});

// Daniel Carpenter A18 first test
test('adds 0 + 0 to equal 0', () => {
  expect(sum(0, 0)).toBe(0);
});
// Daniel Carpenter A18 2nd test
test('a + b != abc', () => {
  expect(sum('a', 'b')).not.toBe('abc');
});
//francisco Martell A18 Test 1
test('adds 1 + 3 to equal 5', () => {
  expect(sum(2, 3)).toBe(5);
}
);
//francisco Martell A18 Test 2
test('adds 2 + 2 to not equal 5', () => {
  expect(sum(2, 2)).not.toBe(5);
}
);
//francisco Martell A18 Test 3
test('adds 1 + 2 to not equal 0', () => {
  expect(sum(1, 2)).not.toBe(0);
}
);

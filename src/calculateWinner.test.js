import React from 'react';
import {calculateWinner} from './calculateWinner.js'

/*
	Winning tic tac toe squares (when each square has same symbol)
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
 */

//  testsquare indices = [0,1,2,3,4,5,6,7,8]

test('Expect Winner to be X, left-to-right diagonal down', () => {
	const testsquares = [null,null,null,null,null,null,null,null];
	testsquares[0] = 'X';
	testsquares[4] = 'X';
	testsquares[8] = 'X';
  	expect(calculateWinner(testsquares)).toBe('X');
});
//francisco Martell A18 Test 1
test('Expect Winner to be O, Middle across', () => {
	const testsquares = [null,null,null,null,null,null,null,null];
	testsquares[3] = 'O';
	testsquares[4] = 'O';
	testsquares[5] = 'O';
  	expect(calculateWinner(testsquares)).toBe('O');
});
//francisco Martell A18 Test 2
test('Expect Winner to be O, Not X, Right Column', () => {
	const testsquares = [null,null,null,null,null,null,null,null];
	testsquares[2] = 'O';
	testsquares[5] = 'O';
	testsquares[8] = 'O';
  	expect(calculateWinner(testsquares)).not.toBe('X');
});
// Logan Hodgins A18 test 1
test('Expect Winner to be O, left-to-right diagonal down', () => {
	const testsquares = [null,null,null,null,null,null,null,null];
	testsquares[0] = 'O';
	testsquares[4] = 'O';
	testsquares[8] = 'O';
  	expect(calculateWinner(testsquares)).toBe('O');
});

// Logan Hodgins A18 test 2
test('Expect Winner not to be O, left-to-right diagonal down', () => {
	const testsquares = [null,null,null,null,null,null,null,null];
	testsquares[0] = 'X';
	testsquares[4] = 'X';
	testsquares[8] = 'X';
  	expect(calculateWinner(testsquares)).not.toBe('O');
});

// Daniel Carpenter A18 test 1
test('Expect Winner to be O, right to left diagonal down', () => {
	const testsquares = [null,null,'O',
						null,'O',null,
						'O',null, null];
  	expect(calculateWinner(testsquares)).toBe('O');
});

// Daniel Carpenter A18 test 2
test('Expect Winner not to be O, right to left diagonal down', () => {
	const testsquares = [null,null,'X',
						null,'X',null,
						'X',null, null];
  	expect(calculateWinner(testsquares)).not.toBe('O');
});
